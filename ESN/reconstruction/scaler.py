"""
    Copyright Roland S. Zimmermann, 2019-2021
"""
import numpy as np


class MinMaxScaler(object):
    def __init__(self, feature_range=(0, 1)):
        self._feature_range = feature_range
        self._max = None
        self._min = None

    def fit(self, x):
        self._max = np.max(x)
        self._min = np.min(x)

    def transform(self, x):
        if self._max is None or self._min is None:
            raise ValueError("Fit has to be called before transform")

        x -= self._min
        x /= self._max - self._min

        x *= self._feature_range[1] - self._feature_range[0]
        x += self._feature_range[0]

        return x

    def fit_transform(self, x):
        self.fit(x)
        return self.transform(x)

    def inverse_transform(self, x):
        x -= self._feature_range[0]
        x /= self._feature_range[1] - self._feature_range[0]

        x *= self._max - self._min
        x += self._min

        return x
