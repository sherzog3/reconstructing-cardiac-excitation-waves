"""
    Copyright Roland S. Zimmermann, 2019-2021
"""

import os
import glob
import numpy as np
from tqdm import tqdm

DATA_DIRECTORY = "/path/to/bocf/"
N_CASES = 10


def load_case(folder_name, length=-1, domain=((0, 512), (0, 512))):
    file_names = sorted(glob.glob(os.path.join(folder_name, "*.npy")))

    if length > 0:
        file_names = file_names[:length]
    elif length < 0:
        file_names = file_names[length:]

    data = np.empty(
        (len(file_names), domain[0][1] - domain[0][0], domain[1][1] - domain[1][0])
    )

    for i, filename in tqdm(
        enumerate(file_names), position=2, leave=False, total=len(file_names)
    ):
        data[i] = np.load(filename)[
            domain[0][0] : domain[0][1], domain[1][0] : domain[1][1]
        ]

    return data


def load_folders(folders, length=-1, domain=((0, 512), (0, 512))):
    data = []
    for i, folder in tqdm(
        enumerate(folders), position=1, leave=False, total=len(folders)
    ):
        data.append(load_case(folder, length, domain))

    return np.stack(data, axis=0)


def load_data_single_case(type, mode, case, length=0, domain=((0, 512), (0, 512))):
    assert type in ("fft", "noise")
    assert mode in ("train", "val", "test")

    data_folders = [os.path.join(DATA_DIRECTORY, "ref", mode)]
    input_directory = os.path.join(DATA_DIRECTORY, type)

    case_folder = os.path.join(input_directory, f"case_{case}", mode)
    data_folders.append(case_folder)

    if not os.path.exists(case_folder):
        raise ValueError("folder does not exist for case:", case, case_folder)

    data = load_folders(data_folders, length, domain)

    output_data = data[0]
    input_data = data[1]

    return input_data, output_data


def load_data_all_cases(type, mode, length=0, domain=((0, 512), (0, 512))):
    assert type in ("fft", "noise")
    assert mode in ("train", "val", "test")

    data_folders = [os.path.join(DATA_DIRECTORY, "ref", mode)]
    input_directory = os.path.join(DATA_DIRECTORY, type)

    for n in range(1, N_CASES + 1):
        case_folder = os.path.join(input_directory, "case_{0}".format(n), mode)

        # dont add folders that do not exist...
        if os.path.exists(case_folder):
            data_folders.append(case_folder)

    data = load_folders(data_folders, length, domain)

    output_data = data[[0]]
    input_data = data[1:]

    # repeat input data now
    output_data = np.repeat(output_data, len(input_data), axis=0)

    return input_data, output_data
