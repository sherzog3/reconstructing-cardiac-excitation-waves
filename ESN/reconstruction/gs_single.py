"""
    Run a grid-search over the hyperparameters of a spatio-temporal ESN
 (except patch size, stride) for a single case.

    Copyright Roland S. Zimmermann, 2019-2021
 """
import easyesn as esn
import utility as ut
import numpy as np
import os
import sys
from scaler import MinMaxScaler
import pickle
import argparse

import multiprocessing
import multiprocess
from io_stream import ForceIOStream

# set the temporary buffer for the multiprocessing module manually to the shm
# to solve "no enough space"-problems
multiprocessing.process.current_process()._config["tempdir"] = "/dev/shm/"
multiprocess.process.current_process()._config["tempdir"] = "/dev/shm/"


def _parse_parameters():
    id = int(os.getenv("SGE_TASK_ID", 1)) - 1

    # hyperparameter combinations (patch size, stride) to test
    values = [
        (25, 2),
        (25, 4),
        (25, 8),
        (25, 24),
        (29, 2),
        (29, 7),
        (29, 14),
        (29, 28),
        (33, 2),
        (33, 8),
        (33, 16),
        (33, 32),
        (37, 2),
        (37, 4),
        (37, 9),
        (37, 12),
        (37, 18),
        (37, 36),
        (41, 2),
        (41, 4),
        (41, 8),
        (41, 20),
        (41, 40),
        (45, 2),
        (45, 4),
        (45, 11),
        (45, 22),
        (45, 44),
        (49, 4),
        (49, 8),
        (49, 16),
        (49, 24),
        (49, 48),
        (101, 10),
        (101, 20),
        (101, 25),
        (101, 50),
    ]

    case_id = id // len(values) + 1
    parameter_id = id % len(values)

    sigma, delta_sigma = values[parameter_id]

    print(
        f"(case {case_id}) using ID {id}/{len(values)} which means: sigma={sigma}, delta_sigma={delta_sigma}"
    )

    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", choices=("noise", "fft"), default="fft", type=str)
    parser.add_argument(
        "--output-folder",
        type=str,
        default="/data.bmp3/roland/esn-denoising/data/single/gridsearch/results",
    )
    args = parser.parse_args()

    return id, case_id, sigma, delta_sigma, args


def main():
    TRAIN_LENGTH = -5000
    VALIDATION_LENGTH = 2500

    center = 512 // 2
    id, case, sigma, delta_sigma, args = _parse_parameters()

    os.makedirs("data", exist_ok=True)
    os.makedirs(args.output_folder, exist_ok=True)

    param_grid = {
        "n_reservoir": [250, 500, 1000],  # , 600, 800],
        "spectralRadius": [
            0.05,
            0.1,
            0.3,
            0.5,
            0.75,
            0.90,
            1.0,
            1.25,
            1.5,
            2.0,
            3.0,
        ],
        "leakingRate": [0.01, 0.05, 0.2, 0.5, 0.7, 0.9, 0.95],  # , 0.99],
        "randomSeed": [42, 41, 40],  # , 39, 38],
        "reservoirDensity": [0.05, 0.1, 0.2],
        # '"noiseLevel": [0.0001, 0.00001],
        "regressionParameters": [[1e1], [5e-0], [5e-1], [5e-2]],
    }

    fixed_params = {
        "n_output": 1,
        "n_input": int(np.ceil(sigma / delta_sigma) ** 2),
        "solver": "lsqr",
        "noiseLevel": 0.0,  # 0.0001,
        # "weightGeneration": "advanced",
        "inputScaling": 1.0,
    }

    # reservoir = esn.PredictionESN(
    #    n_reservoir=1000, **fixed_params, reservoirDensity=0.1, regressionParameters=[1]
    # )
    # reservoir._inputScaling = 1.0
    # reservoir.estimateTransientTime = lambda x, y: 200
    # optimizer = esn.optimizers.GradientOptimizer(reservoir)
    gs = esn.optimizers.GridSearchOptimizer(
        esnType=esn.PredictionESN,
        parametersDictionary=param_grid,
        fixedParametersDictionary=fixed_params,
    )

    print("Detected arguments:")
    for k, v in sorted(vars(args).items()):
        print("\t{0}: {1}".format(k, v))

    print("loading training data...")
    input_training_data, output_training_data = ut.load_data_single_case(
        args.mode,
        "train",
        case=case,
        length=TRAIN_LENGTH,
        domain=(
            (center - sigma // 2, center + sigma // 2 + 1),
            (center - sigma // 2, center + sigma // 2 + 1),
        ),
    )

    print("loading validation data...")
    input_validation_data, output_validation_data = ut.load_data_single_case(
        args.mode,
        "val",
        case=case,
        length=VALIDATION_LENGTH,
        domain=(
            (center - sigma // 2, center + sigma // 2 + 1),
            (center - sigma // 2, center + sigma // 2 + 1),
        ),
    )

    print("input_training_data", input_training_data.shape)
    print("output_training_data", output_training_data.shape)
    print("input_validation_data", input_validation_data.shape)
    print("output_validation_data", output_validation_data.shape)

    # create patches/local states

    input_training_data = input_training_data[:, ::delta_sigma, ::delta_sigma].reshape(
        len(input_training_data), -1
    )
    output_training_data = output_training_data[:, sigma // 2, sigma // 2].reshape(
        len(output_training_data), 1
    )

    input_validation_data = input_validation_data[
        :, ::delta_sigma, ::delta_sigma
    ].reshape(len(input_validation_data), -1)
    output_validation_data = output_validation_data[:, sigma // 2, sigma // 2].reshape(
        len(output_validation_data), 1
    )

    # rescale input and output data
    input_scaler = MinMaxScaler(feature_range=(0, 1))
    output_scaler = MinMaxScaler(feature_range=(0, 1))

    input_training_data = input_scaler.fit_transform(input_training_data)
    output_training_data = output_scaler.fit_transform(output_training_data)

    input_validation_data = input_scaler.transform(input_validation_data)
    output_validation_data = output_scaler.transform(output_validation_data)

    # add dummy "data" dimension because gs expects multie validation data
    # input_validation_data = np.expand_dims(input_validation_data, 0)
    # output_validation_data = np.expand_dims(output_validation_data, 0)

    print("data loaded.")

    results = gs.fit_parallel(
        input_training_data,
        output_training_data,
        input_validation_data,
        output_validation_data,
        # transientTime=500,
        transientTime=500,
        verbose=1,
        n_jobs=32,
    )

    print("GS finished.")
    results_filename = os.path.join(args.output_folder, f"id_{id}.pkl")
    print("saving results to:", results_filename)
    with open(results_filename, "wb") as f:
        pickle.dump(results, f)

    print(gs._best_params)
    print(gs._best_mse)


if __name__ == "__main__":
    sys.stdout = ForceIOStream(sys.stdout)
    sys.stderr = ForceIOStream(sys.stderr)

    main()
