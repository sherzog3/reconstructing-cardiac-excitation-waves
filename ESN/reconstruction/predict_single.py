"""
    Get predictions of a spation-temporal ESN for a single case.

    Copyright Roland S. Zimmermann, 2019-2021
"""

import utility as ut
import numpy as np
import os
import sys
import pickle
import argparse
import easyesn

import multiprocessing
import multiprocess
from io_stream import ForceIOStream

# set the temporary buffer for the multiprocessing module manually to the shm
# to solve "no enough space"-problems
multiprocessing.process.current_process()._config["tempdir"] = "/dev/shm/"
multiprocess.process.current_process()._config["tempdir"] = "/dev/shm/"


def _parse_parameters():
    id = int(os.getenv("SGE_TASK_ID", 1))

    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", choices=("noise", "fft"), default="fft", type=str)
    parser.add_argument(
        "--output-folder",
        type=str,
        default="/data.bmp3/roland/esn-denoising/data/single/fit_256x256/results",
    )
    args = parser.parse_args()

    case_id = id

    return case_id, args


hyperparameters_noise = [
    {
        "n_reservoir": 500,
        "spectralRadius": 1.25,
        "leakingRate": 0.5,
        "randomSeed": 40,
        "reservoirDensity": 0.05,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 500,
        "spectralRadius": 0.5,
        "leakingRate": 0.2,
        "randomSeed": 42,
        "reservoirDensity": 0.05,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 1.0,
        "leakingRate": 0.2,
        "randomSeed": 41,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 0.05,
        "leakingRate": 0.2,
        "randomSeed": 41,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 3.0,
        "leakingRate": 0.2,
        "randomSeed": 42,
        "reservoirDensity": 0.1,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 1.25,
        "leakingRate": 0.2,
        "randomSeed": 41,
        "reservoirDensity": 0.1,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 3.0,
        "leakingRate": 0.2,
        "randomSeed": 42,
        "reservoirDensity": 0.05,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 500,
        "spectralRadius": 1.25,
        "leakingRate": 0.05,
        "randomSeed": 42,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 500,
        "spectralRadius": 3.0,
        "leakingRate": 0.05,
        "randomSeed": 40,
        "reservoirDensity": 0.05,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
]

hyperparameters_fft = [
    {
        "n_reservoir": 250,
        "spectralRadius": 0.3,
        "leakingRate": 0.01,
        "randomSeed": 42,
        "reservoirDensity": 0.1,
        "regressionParameters": [10.0],
        "filterSize": 33,
        "stride": 8,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 1.25,
        "leakingRate": 0.05,
        "randomSeed": 41,
        "reservoirDensity": 0.05,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 4,
    },
    {
        "n_reservoir": 500,
        "spectralRadius": 2.0,
        "leakingRate": 0.05,
        "randomSeed": 42,
        "reservoirDensity": 0.1,
        "regressionParameters": [10.0],
        "filterSize": 41,
        "stride": 4,
    },
    {
        "n_reservoir": 500,
        "spectralRadius": 0.75,
        "leakingRate": 0.2,
        "randomSeed": 40,
        "reservoirDensity": 0.2,
        "regressionParameters": [5.0],
        "filterSize": 29,
        "stride": 2,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 1.5,
        "leakingRate": 0.2,
        "randomSeed": 42,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 4,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 1.5,
        "leakingRate": 0.2,
        "randomSeed": 42,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 4,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 1.25,
        "leakingRate": 0.2,
        "randomSeed": 42,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 4,
    },
    {
        "n_reservoir": 500,
        "spectralRadius": 0.9,
        "leakingRate": 0.2,
        "randomSeed": 42,
        "reservoirDensity": 0.2,
        "regressionParameters": [5.0],
        "filterSize": 25,
        "stride": 2,
    },
    {
        "n_reservoir": 250,
        "spectralRadius": 1.5,
        "leakingRate": 0.5,
        "randomSeed": 42,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 4,
    },
    {
        "n_reservoir": 500,
        "spectralRadius": 0.75,
        "leakingRate": 0.7,
        "randomSeed": 40,
        "reservoirDensity": 0.2,
        "regressionParameters": [10.0],
        "filterSize": 25,
        "stride": 2,
    },
]

weights_filenames_fft = [
    "weights_id_9.pkl",
    "weights_id_38.pkl",
    "weights_id_93.pkl",
    "weights_id_115.pkl",
    "weights_id_149.pkl",
    "weights_id_186.pkl",
    "weights_id_223.pkl",
    "weights_id_259.pkl",
    "weights_id_297.pkl",
    "weights_id_333.pkl",
]

weights_filenames_noise = [
    "weights_id_0.pkl",
    "weights_id_37.pkl",
    "weights_id_74.pkl",
    "weights_id_111.pkl",
    "weights_id_148.pkl",
    "weights_id_185.pkl",
    "weights_id_222.pkl",
    "weights_id_259.pkl",
    "weights_id_296.pkl",
]


def main():
    TRAIN_LENGTH = -5000
    VALIDATION_LENGTH = -200
    TEST_LENGTH = 2500

    width, height = 512, 512
    pseudo_width = 512

    domain_start_end = (width // 2 - pseudo_width // 2, width // 2 + pseudo_width // 2)
    data_domain = (
        (domain_start_end[0], domain_start_end[1]),
        (domain_start_end[0], domain_start_end[1]),
    )
    input_shape = (
        (domain_start_end[1] - domain_start_end[0]),
        (domain_start_end[1] - domain_start_end[0]),
    )

    id, args = _parse_parameters()

    os.makedirs("data", exist_ok=True)
    os.makedirs(args.output_folder, exist_ok=True)

    weights_folder = os.path.join(args.output_folder, "weights")
    assert os.path.exists(weights_folder), "weights could not be found"
    test_prediction_folder = os.path.join(args.output_folder, "test_prediction_512x512")
    os.makedirs(test_prediction_folder, exist_ok=True)

    print("Detected arguments:")
    for k, v in sorted(vars(args).items()):
        print("\t{0}: {1}".format(k, v))

    if args.mode == "fft":
        hyperparameters = hyperparameters_fft
        weights_filenames = weights_filenames_fft
    elif args.mode == "noise":
        hyperparameters = hyperparameters_noise
        weights_filenames = weights_filenames_noise
    else:
        raise ValueError("invalid mode")

    hparams = hyperparameters[id - 1]
    weights_filename = weights_filenames[id - 1]
    weights_filename = os.path.join(weights_folder, weights_filename)

    stesn = easyesn.SpatioTemporalESN(
        inputShape=input_shape,
        nWorkers="auto",
        borderMode="edge",
        solver="lsqr",
        averageOutputWeights=True,
        **hparams,
    )

    print("Using hyperparameters:")
    for k, v in sorted(hparams.items()):
        print("\t{0}: {1}".format(k, v))

    print("loading weights from:", weights_filename)
    with open(weights_filename, "rb") as f:
        weights = pickle.load(f)

    stesn._WInput = weights["WInput"]
    stesn._WOut = weights["WOut"]

    print("loading validation data...")
    input_validation_data, output_validation_data = ut.load_data_single_case(
        args.mode, "val", case=id, length=VALIDATION_LENGTH, domain=data_domain
    )

    print("loading test data...")
    input_test_data, output_test_data = ut.load_data_single_case(
        args.mode, "test", case=id, length=TEST_LENGTH, domain=data_domain
    )

    print("input_test_data", input_test_data.shape)
    print("output_test_data", output_test_data.shape)

    input_scaler = weights["inputScaler"]
    output_scaler = weights["outputScaler"]

    input_validation_data = input_scaler.transform(input_validation_data)
    input_test_data = input_scaler.transform(input_test_data)

    print("data loaded.")

    print("init states properly by predicting on val data...")
    prediction_validation = stesn.predict(
        input_validation_data, transientTime=200, verbose=1
    )

    # training data is not used anymore
    del input_validation_data
    del output_validation_data

    print("predict on test data...")
    prediction_test = stesn.predict(input_test_data, transientTime=0, verbose=1)

    # invert transformation
    prediction_test = output_scaler.inverse_transform(prediction_test)
    print("prediction finished.")

    print("prediction test", prediction_test.shape)

    # calculate MAE/MSE
    difference = prediction_test - output_test_data
    mse = np.mean(difference ** 2)
    mae = np.mean(np.abs(difference))
    print("Test (Full):")
    print("MSE: {0}".format(mse))
    print("MAE: {0}".format(mae))

    # saving test prediction
    print("saving test results...")
    test_prediction_filename = os.path.join(
        test_prediction_folder, f"test_prediction_id_{id}.pkl"
    )
    print("saving test prediction to:", test_prediction_filename)
    with open(test_prediction_filename, "wb") as f:
        pickle.dump(prediction_test, f, protocol=4)
    print("test results saved.")


if __name__ == "__main__":
    sys.stdout = ForceIOStream(sys.stdout)
    sys.stderr = ForceIOStream(sys.stderr)

    main()
