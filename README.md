# Reconstructing cardiac excitation waves

Code associated with the paper: "Reconstructing Complex Cardiac Excitation Waves From Incomplete Data Using Echo State Networks and Convolutional Autoencoders"

This repository is still under construction and is not complete.

The files in the Data folder are only a minimum working set, for running and testing the code.