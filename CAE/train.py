# %%
from models import *
from generators import *
import numpy as np
import argparse

from keras.optimizers import *
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger, TensorBoard, ReduceLROnPlateau
import tensorflow as tf

import os

parser = argparse.ArgumentParser()
parser.add_argument('--CUDA_VISIBLE_DEVICES', type=str,
                    default="0", help='set GPU')

parser.add_argument('--basePath', type=str,
                    default="~/data", help='set path to base directory with training data')

parser.add_argument('--caseName', type=str,
                    default="fft/case_1", help='set case name')

parser.add_argument('--upscaling', type=bool,
                    default=False, help='flag for the undersampled case')

args = parser.parse_args()

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = args.CUDA_VISIBLE_DEVICES
# %%

basePath = args.basePath
refbasePath = basePath + "ref/"
caseName = args.caseName
undersampled = args.upscaling

train_dir = basePath + caseName + "/train/"
val_dir = basePath + caseName + "/val/"
test_dir = basePath + caseName + "/test/"

ref_train_dir = refbasePath + "train/"
ref_val_dir = refbasePath + "val/"
ref_test_dir = refbasePath + "test/"


# adjust this parameters to fit to you hardware and case i.e.: smaller batch size and more filters
BATCH_SIZE = 64
N_FILTERS = 16
EPOCHS = 10000


CASE = "unet_BOCF_" + "_".join(caseName.split("/")) + \
    "_filters_" + str(N_FILTERS)

os.makedirs("results", exist_ok=True)


train_generator = Generator(
    train_dir, ref_train_dir, BATCH_SIZE, shuffle_images=False)

val_generator = Generator(
    val_dir, ref_val_dir, BATCH_SIZE, shuffle_images=False)


callbacks = [ModelCheckpoint("results/weights_" + CASE + ".hdf5", monitor='val_loss', verbose=1, save_best_only=True),
             EarlyStopping(monitor='val_loss', patience=10, min_delta=1e-6), ReduceLROnPlateau(
                 monitor='val_loss', factor=0.2, patience=50, min_lr=0.0001),
             CSVLogger("results/log_" + CASE + ".log")]

# %%
X_train = []
y_train = []

X_val = []
y_val = []


for px, py in zip(train_generator.image_paths, train_generator.refImage_paths):
    x = np.load(px)
    y = np.load(py)
    X_train.append(x.reshape(x.shape[0], x.shape[1], 1))
    y_train.append(y.reshape(y.shape[0], y.shape[1], 1))


for px, py in zip(val_generator.image_paths, val_generator.refImage_paths):
    x = np.load(px)
    y = np.load(py)
    X_val.append(x.reshape(x.shape[0], x.shape[1], 1))
    y_val.append(y.reshape(y.shape[1], y.shape[1], 1))


X_train = np.asarray(X_train)
y_train = np.asarray(y_train)


X_val = np.asarray(X_val)
y_val = np.asarray(y_val)


if(undersampled):
    model = unet_sr(n_filters=N_FILTERS, dropout=0.2, batchnorm=True, nBlocks=int(np.log2(y_train[0].shape[0] / X_train[0].shape[0])), img_shape=(
        int(X_train.shape[1]), int(X_train.shape[2]), int(1)))
else:
    model = unet(n_filters=N_FILTERS, img_shape=(
        X_train[0].shape[0], X_train[0].shape[1], 1))


model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-3),
              loss='mean_absolute_error', metrics=['mae'])


history = model.fit(X_train, y_train, epochs=EPOCHS, callbacks=callbacks,
                    batch_size=BATCH_SIZE, validation_data=(X_val, y_val))


model.save("results/model_" + CASE + ".hdf5")


# %%
