from tensorflow.keras.layers import Conv2D, BatchNormalization, Input, Dropout, Conv2DTranspose, concatenate, add, LeakyReLU, PReLU, Activation, MaxPooling2D, Dense, Reshape, Concatenate
from tensorflow.keras.models import *


def conv2d_block(input_tensor, n_filters, kernel_size=3, batchnorm=True):
    # first layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size),
               kernel_initializer="he_normal", padding="same")(input_tensor)
    if batchnorm:
        x = BatchNormalization()(x)
    x = LeakyReLU(alpha=0.3)(x)

    # second layer
    x = Conv2D(filters=n_filters, kernel_size=(kernel_size, kernel_size),
               kernel_initializer="he_normal", padding="same")(x)
    if batchnorm:
        x = BatchNormalization()(x)

    x = LeakyReLU(alpha=0.3)(x)

    return x


def unet(n_filters=16, dropout=0.05, batchnorm=True, img_shape=(512, 512, 1)):

    input = Input(shape=(img_shape[0], img_shape[1], img_shape[2]))

    # contracting path
    c1 = conv2d_block(input, n_filters=n_filters*1,
                      kernel_size=3, batchnorm=batchnorm)
    p1 = MaxPooling2D((2, 2))(c1)
    p1 = Dropout(rate=dropout)(p1)

    c2 = conv2d_block(p1, n_filters=n_filters*2,
                      kernel_size=3, batchnorm=batchnorm)
    p2 = MaxPooling2D((2, 2))(c2)
    p2 = Dropout(rate=dropout)(p2)

    c3 = conv2d_block(p2, n_filters=n_filters*3,
                      kernel_size=3, batchnorm=batchnorm)
    p3 = MaxPooling2D((2, 2))(c3)
    p3 = Dropout(rate=dropout)(p3)

    c4 = conv2d_block(p3, n_filters=n_filters*4,
                      kernel_size=3, batchnorm=batchnorm)
    p4 = MaxPooling2D(pool_size=(2, 2))(c4)
    p4 = Dropout(rate=dropout)(p4)

    c5 = conv2d_block(p4, n_filters=n_filters*5,
                      kernel_size=3, batchnorm=batchnorm)

    # expansive path
    u6 = Conv2DTranspose(n_filters*4, (3, 3),
                         strides=(2, 2), padding='same')(c5)
    u6 = concatenate([u6, c4])
    u6 = Dropout(rate=dropout)(u6)
    c6 = conv2d_block(u6, n_filters=n_filters*4,
                      kernel_size=3, batchnorm=batchnorm)

    u7 = Conv2DTranspose(n_filters*3, (3, 3),
                         strides=(2, 2), padding='same')(c6)
    u7 = concatenate([u7, c3])
    u7 = Dropout(rate=dropout)(u7)
    c7 = conv2d_block(u7, n_filters=n_filters*3,
                      kernel_size=3, batchnorm=batchnorm)

    u8 = Conv2DTranspose(n_filters*2, (3, 3),
                         strides=(2, 2), padding='same')(c7)
    u8 = concatenate([u8, c2])
    u8 = Dropout(rate=dropout)(u8)
    c8 = conv2d_block(u8, n_filters=n_filters*2,
                      kernel_size=3, batchnorm=batchnorm)

    u9 = Conv2DTranspose(n_filters*1, (3, 3),
                         strides=(2, 2), padding='same')(c8)
    u9 = concatenate([u9, c1], axis=3)
    u9 = Dropout(rate=dropout)(u9)
    c9 = conv2d_block(u9, n_filters=n_filters,
                      kernel_size=1, batchnorm=batchnorm)
    outputs = Conv2D(1, (1, 1), activation='relu')(c9)
    model = Model(inputs=input, outputs=outputs)
    return model


def unet_sr(n_filters=16, dropout=0.05, batchnorm=True, nBlocks=6, img_shape=(512, 512, 1)):

    input = Input(shape=(img_shape[0], img_shape[1], 1))

    u = conv2d_block(input, n_filters=n_filters,
                     kernel_size=3, batchnorm=batchnorm)
    for block in range(nBlocks):
        if(block < 2):
            filters = int(n_filters/(2**block))
        else:
            filters = int(10)

        u = Conv2DTranspose(filters, (3, 3),
                            strides=(2, 2), padding='same')(u)
        u = Dropout(rate=dropout)(u)
        u = conv2d_block(u, n_filters=filters,
                         kernel_size=3, batchnorm=batchnorm)

    outputs = Conv2D(1, (1, 1), activation='relu')(u)

    model = Model(inputs=input, outputs=outputs)
    return model
