import os
import numpy as np

from sklearn import preprocessing
import tensorflow as tf
from keras_preprocessing import image
import glob
import threading


class Generator(tf.keras.utils.Sequence):

    def __init__(self, DATASET_PATH, REF_PATH, BATCH_SIZE=32, shuffle_images=True):
        """ Initialize Generator object.
        Args
            DATASET_PATH           : Path to folder containing individual folders named by their class names
            BATCH_SIZE             : The size of the batches to generate.
            shuffle_images         : If True, shuffles the images read from the DATASET_PATH
        """

        self.batch_size = BATCH_SIZE
        self.shuffle_images = shuffle_images
        self.load_image_paths(DATASET_PATH, REF_PATH)
        self.create_image_groups()
        self.n = 0
        self.max = self.__len__()

    def load_image_paths(self, DATASET_PATH, REF_PATH):

        self.image_paths = []
        self.refImage_paths = []
        for image_file_name in glob.glob(DATASET_PATH+"*npy"):
            self.image_paths.append(image_file_name)
            self.refImage_paths.append(REF_PATH+image_file_name.split("/")[-1])

    def create_image_groups(self):
        if self.shuffle_images:
            # Randomly shuffle dataset
            seed = 4321
            np.random.seed(seed)
            c = list(zip(self.image_paths, self.refImage_paths))
            np.random.shuffle(c)
            self.image_paths, self.refImage_paths = zip(*c)

        # Divide image_paths and image_labels into groups of BATCH_SIZE
        self.image_groups = [[self.image_paths[x % len(self.image_paths)] for x in range(i, i + self.batch_size)]
                             for i in range(0, len(self.image_paths), self.batch_size)]

        self.image_groups_ref = [[self.refImage_paths[x % len(self.refImage_paths)] for x in range(i, i + self.batch_size)]
                                 for i in range(0, len(self.refImage_paths), self.batch_size)]

    def load_images(self, image_group):

        images = []
        temp = np.load(image_group[0])
        for image_path in image_group:
            images.append(np.load(image_path).reshape(
                temp.shape[0], temp.shape[1], 1))
        return images

    def construct_image_batch(self, image_group):
        # get the max image shape

        # max_shape = tuple(max(image.shape[x]
        #                       for image in image_group) for x in range(3))

        max_shape = image_group[0].shape

        # construct an image batch object
        image_batch = np.zeros((self.batch_size,) + max_shape, dtype='float32')

        # copy all images to the upper left part of the image batch object
        for image_index, image in enumerate(image_group):
            image_batch[image_index] = image

        return image_batch

    def __len__(self):
        """
        Number of batches for generator.
        """

        return len(self.image_groups)

    def __getitem__(self, index):
        """
        Keras sequence method for generating batches.
        """
        image_group = self.image_groups[index]
        images = self.load_images(image_group)
        image_batch = self.construct_image_batch(images)

        image_group_ref = self.image_groups_ref[index]
        images_ref = self.load_images(image_group_ref)
        image_batch_ref = self.construct_image_batch(images_ref)

        return np.array(image_batch), np.array(image_batch_ref)

    def __next__(self):
        if self.n >= self.max:
            self.n = 0
        result = self.__getitem__(self.n)
        self.n += 1
        return result
